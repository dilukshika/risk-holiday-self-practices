package Question24;

public class UnderstandingStringMethods {
	
	public void indexOf1 (String pat,String w) {
		System.out.println("IndexOF1 = " + w.indexOf(pat));
	}
 
	public void indexOf2 (String pat, String w) {
		System.out.println("IndexOf2= " + w.indexOf(pat,3));
	}
	
	public void indexOf3 (String w, String pat) {
		System.out.println("Index3= " + w.indexOf(pat,6));
	}
	
	public void lastIndexOf (String w, String pat) {
		System.out.println("Last IndexOf = " + w.lastIndexOf(pat));
	}
	
	public void length(String w, String pat) {
		System.out.println("Length = " + w.length());
	}
	
	public void upperCase(String w, String pat) {
		System.out.println("UpperCase = " + w.toUpperCase());
	}
	
	public void charAt(String w,String pat) {
		System.out.println("CharAt = " + w.charAt(0));
	}
}
