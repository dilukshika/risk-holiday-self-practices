package Question24;

public class UnderstandingStringMethodsDemo {
	
	public static void main(String[] args) {
		UnderstandingStringMethods demo = new UnderstandingStringMethods();
		
		
		String w = "\"Singin�_in_the_rain\"";
		String pat = "in";
		
		
		
		demo.indexOf1 (w,pat);
		demo.indexOf2 (w,pat);
		demo.indexOf3 (w,pat);
		demo.lastIndexOf(w, pat);
		demo.length(w, pat);
		demo.upperCase(w, pat);
		demo.charAt(w, pat);
				
}
}
/*
output

IndexOF1 = -1
IndexOf2= -1
Index3= 9
Last IndexOf = 18
Length = 21
UpperCase = "SINGIN�_IN_THE_RAIN"
CharAt = "

*/