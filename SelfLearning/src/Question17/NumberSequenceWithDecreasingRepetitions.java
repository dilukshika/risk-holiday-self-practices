package Question17;


public class NumberSequenceWithDecreasingRepetitions {

	public static void main(String[] args) {
		
		
		int row = 9;
		
		for (int i=1; i<=row; i++) {
			
			for (int j=row/2; j>=0; j--) {
				
				for (int k=1; k<=i; k++) {
					System.out.print(j);
				}
			}
			System.out.println(" ");
		}
	}

}
/*
 output
43210 
4433221100 
444333222111000 
44443333222211110000 
4444433333222221111100000 
444444333333222222111111000000 
44444443333333222222211111110000000 
4444444433333333222222221111111100000000 
444444444333333333222222222111111111000000000 
 
 */
