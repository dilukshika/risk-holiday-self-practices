package Question36;

public class ConvertToDoWhileLoop {
	
	public static void main(String args []) {
	
		 int index = 1;
		
		
		do {
			index = index + 3 ;
			System.out.println(index);
		}
		
		while (index <= 15);
	}

}
/*
output
4
7
10
13
16

*/