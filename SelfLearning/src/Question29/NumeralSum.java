package Question29;

import java.util.Scanner;

public class NumeralSum {

	public static void main (String args[]) {
		
			Scanner scan = new Scanner(System.in);
			System.out.print("Enter Input: ");
			String word = scan.nextLine();
			
			int word1 = word.length();
			int sum = 0;
			
			
			for (int i = 0; i < word1; i++) {
				
				if (Character.isDigit(word.charAt(i))) {
					sum = sum + Character.getNumericValue(word.charAt(i));
					System.out.print(Character.getNumericValue(word.charAt(i)));
				}
			}
			System.out.println(" Sum Is : "+ sum);
		
	}

}
/*
output
Enter Input:  BIO542L
542 Sum Is : 11
*/