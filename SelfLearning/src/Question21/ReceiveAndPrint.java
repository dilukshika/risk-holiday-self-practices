package Question21;

import java.util.Scanner;

public class ReceiveAndPrint {
	// Question 21
	public static void main (String args []) {
		
		Scanner scan=new Scanner(System.in);
		System.out.print("Enter String: ");
		String s=scan.nextLine();
		
		System.out.print("Enter int: ");
		int m=scan.nextInt();

		System.out.print("Enter double: ");
		double d=scan.nextDouble();
		
		System.out.println(s);
		System.out.println(m);
		System.out.println(d);
		System.out.println(m + d + s);
		System.out.println(m + s + d);
		System.out.println(s + m + d);
	
		
	}
}
/*
output

Enter double: 54.8
java
21
54.8
75.8java
21java54.8
java2154.8
*/