package Question31;

import java.util.Scanner;

public class ToDecimal {
	
	public static void main (String args[]) {
		
		long binaryNumber ;
		long decimalNumber = 0;
		long i = 1;
		long num;
		
		  Scanner scan = new Scanner(System.in);
		  System.out.print("Enter The Binary Number: ");
		  binaryNumber = scan.nextLong();

		  while (binaryNumber != 0) {
		  
		   num = binaryNumber % 10;
		   decimalNumber = decimalNumber + num * i;
		   i = i * 2;
		   binaryNumber = binaryNumber / 10;
		  }
		  System.out.println("Decimal Number: " + decimalNumber );
		 }
		
	}
/*
 output 
Enter The Binary Number: 110011110
Decimal Number: 414
 */

