package Question04And05;

public class SlashPattern {
	public static void main (String args []) {
		
		 int rows=10;
		 
			for (int i = 1; i <= rows; i++) {
				System.out.print(i + " ");
				
				if (i % 2 == 1) {
					for (int j = 1; j <= (rows +2); j++) {
						System.out.print(  "/ ");
						if (j % (rows + 2) == 0) {
							System.out.println(" ");
						}
					}
				} else {
					for (int k = 1; k <= (rows + 1); k++) {
						System.out.print(" /");
						if (k % (rows + 1) == 0) {
							System.out.println( " "); 
						}
					}
}
			}
	}
}

/*
 output
 
1 / / / / / / / / / / / /  
2  / / / / / / / / / / / 
3 / / / / / / / / / / / /  
4  / / / / / / / / / / / 
5 / / / / / / / / / / / /  
6  / / / / / / / / / / / 
7 / / / / / / / / / / / /  
8  / / / / / / / / / / / 
9 / / / / / / / / / / / /  
10  / / / / / / / / / / / 

 */

