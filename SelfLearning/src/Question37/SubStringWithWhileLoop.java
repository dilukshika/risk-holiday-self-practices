package Question37;

public class SubStringWithWhileLoop {
	
	
	public static void main(String[] args) {
		String word = "sebastian-ibis";
		int wordLength = word.length();
		int i = wordLength - 1;
		
		
		while (i > 1) {
			System.out.println(word.substring(i));
		
		i--;
		}
	}
}
/*
 output
s
is
bis
ibis
-ibis
n-ibis
an-ibis
ian-ibis
tian-ibis
stian-ibis
astian-ibis
bastian-ibis

 */

