package Question23;

public class WordStringDemo {
	
	public static void main(String[] args) {
		WordString demo = new WordString();
		
		String word = "\"School.of.Progressive.Rock\"" ;
		
		
		demo.length (word);
		demo.substring (word);
		demo.subString1(word);
		demo.indexOf(word);
		demo.upperCase(word);
		demo.lastIndexOf(word);
		demo.indexOf1(word);
		
	}
}
/*

output

Word Length = 28
word SubString = .Rock"
word subString = .R
word IndexOf = 4
word UpperCase = "SCHOOL.OF.PROGRESSIVE.ROCK"
word Last indexOf = 24
word IndexOf = -1

*/
