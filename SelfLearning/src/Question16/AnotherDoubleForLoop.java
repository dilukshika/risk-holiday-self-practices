package Question16;


public class AnotherDoubleForLoop {
	public static void main(String args[]) {
		
		
		int row = 10;
		
	
			for ( int i = 0; i<row/2; i++) {
				
				for (int j = row-i-1; j>i; j--) {
					System.out.print( j );
				}
				System.out.println(" ");
			}
		}
}
/*
 output
 
987654321 
8765432 
76543 
654 
5 

*/
