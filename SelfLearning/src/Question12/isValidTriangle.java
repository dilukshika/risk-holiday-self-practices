package Question12;

public class isValidTriangle {
	
	public static void main(String[] args) {
		
		double sideA=7.0;
		double sideB=5.0; 
		double sideC=10.0;

		boolean status= isValidTriangle(sideA, sideB, sideC);
		if(status) {
			System.out.println("ABC is a valid triangle");
		}else {
			System.out.println("ABC is not a valid triangle");
		}
	}

	public static boolean isValidTriangle(double sideA, double sideB, double sideC) {

		double ab = sideA + sideB;
		double ac = sideA + sideC;
		double bc = sideB + sideC;

		if ((sideA < bc) && (sideB < ac) && (sideC < ab)) {
			return true;
		}
		return false;

}
}
/*
 output
 ABC is a valid triangle
 */
