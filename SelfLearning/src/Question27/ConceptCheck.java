package Question27;

public class ConceptCheck {
	
	
		public void length(String s) {
			System.out.println("Length = " + s.length());
		}
		
		public void indexOf(String s) {
			System.out.println("IndexOf = " + s.indexOf("si"));
		}
		
		public void upperCaseAndIndexOd (String s) {
			System.out.println("UpperCase And IndexOf = " + s.toUpperCase().indexOf( "si" ));
		}
		
		public void lowerCaseAndIndexOf (String s) {
			System.out.println("LowerCase And IndexOf = " + s.toLowerCase().indexOf("si"));
		}
		
		public void subStringAndIndexOf(String s) {
			System.out.println("SubString And IndexOf = " + s.substring(0,s.indexOf( "i" )));
		}
		
		public void subStringAndLaseIndexOf (String s) {
			System.out.println("SubString And LastIndexOf = " + s.substring( s.lastIndexOf( "i" ) ));
		}
		 
		
}