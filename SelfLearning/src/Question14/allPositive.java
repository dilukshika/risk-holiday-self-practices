package Question14;

public class allPositive {

	public static void main (String args[]) {
		
		double valueA=4.0;
		double valueB=3.0; 
		double valueC=5.0;

		boolean status= isValidTriangle(valueA, valueB, valueC);
		if(status) {
			System.out.println("All Strictly Positive");
		}else {
			System.out.println("All Strictly Negative");
		}
	}

	public static boolean isValidTriangle(double valueA, double valueB, double valueC) {

		if ((valueA > 0) && (valueB > 0) && (valueC > 0)){
			return true;
		}
			return false;
	}

}
/*
output
All Strictly Positive
*/