package Question09;

import java.util.Scanner;
public class PlayWithNumbersDecomposed {
	
	public void TwoInteger(int a,int b,int sum, int sub, int mul, int divi, int mod ) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("a  = ");
		a =scan.nextInt();
		
		Scanner scan1 = new Scanner(System.in);
		System.out.print("b  = ");
		b =scan.nextInt();
		
		 sum = a+b ;
		 sub = a-b;
		 mul = a*b;
		 divi = a/b;
		 mod = a%b;
		 
		 System.out.println(" a+b is equal to " + sum);
		 System.out.println(" a-b is equal to " + sub);
		 System.out.println(" a*b is equal to " + mul);
		 System.out.println(" a/b is equal to " + divi);
		 System.out.println(" a%b is equal to " + mod);
	}
	
	
	public void ThreeInteger(int a,int b,int c,int anwser1, int anwser2, int anwser3, int anwser4, int anwser5, int anwser6 ) {
		Scanner scan = new Scanner(System.in);
		System.out.print("a  = ");
		a =scan.nextInt();
		
		Scanner scan1 = new Scanner(System.in);
		System.out.print("b  = ");
		b =scan.nextInt();
		
		Scanner scan2 = new Scanner(System.in);
		System.out.print("c  = ");
		c =scan.nextInt();
		
		anwser1 = (a-b)/c;
		anwser2 = (a-c)/b;
		anwser3 = (b-c)/a;
		anwser4 = (b-a)/c;
		anwser5 = (c-a)/b;
		anwser6 = (c-b)/a;
		
		System.out.println(" (a-b)/c is equal to " + anwser1);
		System.out.println(" (a-c)/b is equal to " + anwser2);
		System.out.println(" (b-c)/a is equal to " + anwser3);
		System.out.println(" (b-a)/c is equal to " + anwser4);
		System.out.println(" (c-a)/b is equal to " + anwser5);
		System.out.println(" (c-b)/a is equal to " + anwser6);
		
		
	}

}
