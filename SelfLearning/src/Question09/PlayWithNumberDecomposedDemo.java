package Question09;


public class PlayWithNumberDecomposedDemo {
	public static void main (String args[]) {
		
		int a=0,b=0,sum=0,sub=0,mul=0,divi=0,mod=0;
		int  c=0, anwser1=0, anwser2=0, anwser3=0, anwser4=0, anwser5=0, anwser6=0 ;
		
		PlayWithNumbersDecomposed demo = new PlayWithNumbersDecomposed();
		
		demo.TwoInteger(a,b,sum,sub,mul,divi,mod);
		demo.ThreeInteger(a, b, c, anwser1, anwser2, anwser3, anwser4, anwser5, anwser6);

	}

}
/*
a  =  1000435 
b  = 345
 a+b is equal to 1000780
 a-b is equal to 1000090
 a*b is equal to 345150075
 a/b is equal to 2899
 a%b is equal to 280
a  = 34325
b  = 79
c  = -40
 (a-b)/c is equal to -856
 (a-c)/b is equal to 435
 (b-c)/a is equal to 0
 (b-a)/c is equal to 856
 (c-a)/b is equal to -435
 (c-b)/a is equal to 0

*/