package Question33;

public class WhileLoopForNumbersWithLeading0s {
	
	public static void main (String args[]) {
	
		int i = 1;
		int num = 20;
		int size = num/2 ;
		int num2;
		
		while(i <= size ) {
			num2 = i*2;
			String Zero = String.format("%04d",num2 );
	
			 System.out.println( Zero+":");
			 
			 i++;
		}
	}

}
/*
output
0002:
0004:
0006:
0008:
0010:
0012:
0014:
0016:
0018:
0020:

*/