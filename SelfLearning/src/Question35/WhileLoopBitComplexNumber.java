package Question35;

public class WhileLoopBitComplexNumber {
	
	public static void main(String args []) {
		
		int num=10;
		
		while(num>= 0) {
			
			int size = (int) Math.pow(2,num);
			String zero = String.format("%04d",size);
			System.out.println("+" + zero +"?");
			
			
			num--;
		}
	}

}
/*
 output
+1024?
+0512?
+0256?
+0128?
+0064?
+0032?
+0016?
+0008?
+0004?
+0002?
+0001?

 */
