
public class FindArea {

	public static double reactangle(double width, double height) {
		return width * height;
	}
	
	
	public static double circle(double radious) {
		double area = 22/7 * Math.pow(radious,  2);
		return area;
	}
	
	
	public static double triangle(double base, double height) {
		return 0.5* base* height;
	}
	
	
	public static double squre(double length) {
		return Math.pow(length, 2);
	}
	
	public static int trapezoid(int topBase, int bottomBase, int height) {
		return (topBase + bottomBase) /2 * height;
	}
}
