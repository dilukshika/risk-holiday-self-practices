import org.junit.Test;

import junit.framework.Assert;

public class FindAreaTest {
	
	@Test 
	public void reactangle_Test() {
		double width= 8.0;
		double height = 10.0;
		double expected = 80.0;
		double actual =FindArea.reactangle(width, height);
		
		Assert.assertEquals(expected,actual);
	}
	
	@Test 
	public void circle_Test() {
		Assert.assertEquals(147.0, FindArea.circle(7));
	}
	
	@Test
	public void triangle_Test() {
		Assert.assertEquals(20.0, FindArea.triangle(5, 8));
	}
	
	@Test 
	public void triangle_Test_WithZero() {
		Assert.assertEquals(0.0, FindArea.triangle(0, 8));
	}

	@Test 
	public void squre_Test() {
		Assert.assertEquals(625.0, FindArea.squre(25));
	}

	@Test 
	public void trapezoid_Test() {
		Assert.assertEquals(27, FindArea.trapezoid(8, 10, 3));
	}
	
	@Test 
	public void trapezoid_Test_withNegative() {
		Assert.assertEquals(6, FindArea.trapezoid(9,-5, 3));
	}

}

