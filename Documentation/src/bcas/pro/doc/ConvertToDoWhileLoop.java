package bcas.pro.doc;


/**
 * To perform basic do while loop functions
 * @author Dilukshika
 * @since 17-05-2020
 * @version 1.0.0v
 */
public class ConvertToDoWhileLoop {
	
	
	/**
	 * This is the main method of this program
	 * @param args for input arguments
	 */
	public static void main(String args []) {
		
	/**
	 * variable index , number 1 value
	 */
		 int index = 1;
	/**
	 * to add index number.
	 *  Will return 4,7,10,13,16
	 *  write the Statements in this part
	 */
		
		do {
			index = index + 3 ;
			System.out.println(index);
		}
		
		/**
		 * Boolean_expression
		 */
		
		while (index <= 15);
	}

}
